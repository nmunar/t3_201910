package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class TestStack {


	private Stack<Integer> numeros;

	@Before
	public void setUp() throws Exception{

		numeros  = new Stack<Integer>();

		numeros.push(0);
		numeros.push(1);
		numeros.push(2);
		numeros.push(3);


	}

	@Test
	public void test() {

		assertEquals(4, numeros.size());

		assertEquals(3, numeros.pop().intValue());

		numeros.push(4);
		assertEquals(4, numeros.size());


	}

}
