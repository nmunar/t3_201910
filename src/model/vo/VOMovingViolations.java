package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>{
	
	
	private int objectId;
	
	private String location;
	
	private String ticketIssueDate;
	
	private int totalPaid;
	
	private String accidentIndicator;
	
	private String violationDescription;
	
	
	public VOMovingViolations(String pObjectId, String pLocation, String pTicketIssueDate, String pTotalPaid, String pAccidentIndicator, String pViolationDescription) {
		
		objectId = Integer.parseInt(pObjectId);
		location = pLocation;
		ticketIssueDate = pTicketIssueDate;
		totalPaid = Integer.parseInt(pTotalPaid);
		accidentIndicator = pAccidentIndicator;
		violationDescription = pViolationDescription;
		
	}

	
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}


	@Override
	public int compareTo(VOMovingViolations o) {
		// TODO Auto-generated method stub
		
		int respuesta = this.objectId() - o.objectId();
		
		if(respuesta > 0)  {
			
			return 1;
			
		} else if(respuesta < 0) {
			
			return -1;
			
		} else {
			
			return 0;
			
		}
	}
}
