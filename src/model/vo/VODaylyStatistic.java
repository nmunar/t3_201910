package model.vo;

public class VODaylyStatistic implements Comparable<VODaylyStatistic>
{


	private String fechadelDia;

	private String accidenteIndicador;

	private int cantidadAccidentes;

	private int sumaFINEAMT;

	private int cantInfracciones;



	public VODaylyStatistic(String pFechaDelDia, String pAccidenteIndicador, int pSumaFINEAMT, int infracciones, int cantAccidentes) 
	{

		fechadelDia = pFechaDelDia;

		accidenteIndicador = pAccidenteIndicador;

		sumaFINEAMT = pSumaFINEAMT;

		cantInfracciones = infracciones;

		cantidadAccidentes = cantAccidentes;

	}

	public String darFechaDia()
	{
		return fechadelDia;
	}

	public String darSiHayAccidentes()
	{
		return accidenteIndicador;
	}


	public int darSumaTotalFINEAMT()
	{
		return sumaFINEAMT;
	}

	public int darCantInfracciones()
	{
		return cantInfracciones;
	}

	public int darCantAccidentes()
	{
		return cantidadAccidentes;
	}







	@Override
	public int compareTo(VODaylyStatistic o) 
	{
		int respuesta = Integer.parseInt(this.darFechaDia()) - Integer.parseInt(o.darFechaDia());

		if(respuesta > 0)  {

			return 1;

		} else if(respuesta < 0) {

			return -1;

		} else {

			return 0;

		}
	}



}
