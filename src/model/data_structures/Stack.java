package model.data_structures;

import java.util.Iterator;

public class Stack<T extends Comparable<T>> implements IStack<T>{


	private Node<T> primero;
	private Node<T> ultimo;
	private Node<T> actual;

	private int longitud; 


	public Stack() {
		primero = null;
		ultimo = null;
		actual = primero;
		longitud = 0;

	}


	public Node<T> darPrimero() {

		return primero;


	}

	public Node<T> darActual() {

		return actual;

	}


	@Override
	public Iterator<T> iterator() {
		return new IteratorSQ<>(primero);
	}

	@Override
	public boolean isEmpty() {
		if(primero == null) {

			return true;

		} else {

			return false;

		}

	}

	@Override
	public int size() {
		return longitud;
	}

	@Override
	public void push(T t) {
		Node<T> nuevo = new Node<T>(t);

		if(primero == null) {

			primero = nuevo;

		} else {

			nuevo.cambiarSiguiente(primero);
			primero.cambiarAnterior(nuevo);
			primero = nuevo;
			
		}
		longitud ++;
	}

	@Override
	public T pop() 
	{
		Node<T> eliminado = primero;
		
		if(eliminado != null) {
			
			primero.darSiguiente().cambiarAnterior(null);
			primero = primero.darSiguiente();

			longitud --;
			
		}
		return eliminado.darElemento();
	}

}
