package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import api.IMovingViolationsManager;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class MovingViolationsManager implements IMovingViolationsManager {


	private Stack<VOMovingViolations> stackInfracciones = new Stack<VOMovingViolations>();


	private Queue<VODaylyStatistic> queueInfracciones = new Queue<VODaylyStatistic>();


	String objectId = "";
	String location = "";
	String ticketIssueDate ="";
	String totalPaid ="";
	String accidentIndicator ="";
	String violationDescription ="";
	int sumaFINEAMT =0;



	public void loadMovingViolations(String movingViolationsFile) {
		try {

			String archCSV = "./data/Moving_Violations_Issued_in_January_2018_ordered.csv";


			for(int i = 0; i <= 1; i++) {
				CSVReader csvReader = new CSVReader(new FileReader(archCSV));
				String[] fila = null;
				csvReader.readNext();
				while((fila = csvReader.readNext()) != null) {

					objectId = fila[0];
					location = fila[2];
					ticketIssueDate = fila[13];
					sumaFINEAMT = Integer.parseInt(fila[8]);
					totalPaid = fila[9];
					accidentIndicator = fila[12];
					violationDescription = fila[15];

					//Se crea el objeto VOMovingViolations

					VOMovingViolations infraccion = new VOMovingViolations(objectId, location, ticketIssueDate, totalPaid, accidentIndicator, violationDescription);

					//Se crea el objeto VODaylyStatistic

					VODaylyStatistic pInfraccion = new VODaylyStatistic(ticketIssueDate, accidentIndicator, sumaFINEAMT,1,1);


					//Se agrega el objeto

					stackInfracciones.push(infraccion);
					queueInfracciones.enqueue(pInfraccion);

				}
				csvReader.close();

				archCSV = "./data/Moving_Violations_Issued_in_February_2018_ordered.csv";

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Override
	public Queue <VODaylyStatistic> getDalyStatistics() 
	{

		Iterator<VODaylyStatistic> iter = queueInfracciones.iterator();
		Queue<VODaylyStatistic> resultado = new Queue<VODaylyStatistic>();

		String fecha = null;
		int accidente = 0;
		int sumaTotalAMT=0;
		int infracciones =0;


		while(iter.hasNext())
		{
			VODaylyStatistic daily = iter.next();
			fecha = daily.darFechaDia();
			while(fecha.substring(0,10).equals(daily.darFechaDia().substring(0,10))&&iter.hasNext())
			{
				if(daily.darSiHayAccidentes().equals("Yes"))
					accidente++;

				sumaTotalAMT+=daily.darSumaTotalFINEAMT();

				infracciones++;

				daily = iter.next();
			}
			VODaylyStatistic nuevo = new VODaylyStatistic(fecha, daily.darSiHayAccidentes(), sumaTotalAMT, infracciones+1, accidente);
			resultado.enqueue(nuevo);

			accidente = 0;
			sumaTotalAMT=0;
			infracciones =0;

		}

		return resultado;
	}


	@Override
	public Stack <VOMovingViolations> nLastAccidents(int n){
		// TODO Auto-generated method stub
		Stack<VOMovingViolations> elementosN = new Stack<VOMovingViolations>();

		Iterator<VOMovingViolations> it = stackInfracciones.iterator();

		VOMovingViolations ultimo= null;
		int cont = 0;
		while(it.hasNext() && cont < n) {
			
			VOMovingViolations actual = it.next();

			elementosN.push(actual);

			cont++;
		}
		
		Stack<VOMovingViolations> elementosN2 = new Stack<VOMovingViolations>();
		Iterator<VOMovingViolations> it2 = elementosN.iterator();
		
		while(it2.hasNext()) {
			
			VOMovingViolations actual = it2.next();
			
			elementosN2.push(actual);
			
			
		}


		return elementosN2;

	}

}
